//Udacity HW 6
//Poisson Blending

/* Background
   ==========

   The goal for this assignment is to take one image (the source) and
   paste it into another image (the destination) attempting to match the
   two images so that the pasting is non-obvious. This is
   known as a "seamless clone".

   The basic ideas are as follows:

   1) Figure out the interior and border of the source image
   2) Use the values of the border pixels in the destination image 
      as boundary conditions for solving a Poisson equation that tells
      us how to blend the images.
   
      No pixels from the destination except pixels on the border
      are used to compute the match.

   Solving the Poisson Equation
   ============================

   There are multiple ways to solve this equation - we choose an iterative
   method - specifically the Jacobi method. Iterative methods start with
   a guess of the solution and then iterate to try and improve the guess
   until it stops changing.  If the problem was well-suited for the method
   then it will stop and where it stops will be the solution.

   The Jacobi method is the simplest iterative method and converges slowly - 
   that is we need a lot of iterations to get to the answer, but it is the
   easiest method to write.

   Jacobi Iterations
   =================

   Our initial guess is going to be the source image itself.  This is a pretty
   good guess for what the blended image will look like and it means that
   we won't have to do as many iterations compared to if we had started far
   from the final solution.

   ImageGuess_prev (Floating point)
   ImageGuess_next (Floating point)

   DestinationImg
   SourceImg

   Follow these steps to implement one iteration:

   1) For every pixel p in the interior, compute two sums over the four neighboring pixels:
      Sum1: If the neighbor is in the interior then += ImageGuess_prev[neighbor]
             else if the neighbor in on the border then += DestinationImg[neighbor]

      Sum2: += SourceImg[p] - SourceImg[neighbor]   (for all four neighbors)

   2) Calculate the new pixel value:
      float newVal= (Sum1 + Sum2) / 4.f  <------ Notice that the result is FLOATING POINT
      ImageGuess_next[p] = min(255, max(0, newVal)); //clamp to [0, 255]


    In this assignment we will do 800 iterations.
   */



#include "utils.h"
#include <thrust/host_vector.h>
#include "reference_calc.cpp"

#include <stdio.h>

#define true 1
#define false 0

#define interior 50
#define border 60
#define exterior 70

const unsigned int THREAD_PER_BLOCK_1D = 16;

typedef struct ImageBuffer {
    float x;
    float y;
    float z;
} imgBuffer;

unsigned int divideAndRoundUp(unsigned int dividend, unsigned int divisor) {
    unsigned int output = dividend / divisor;
    if (divisor * output < dividend) {
        output++;
    }
    return output;
}

__device__ unsigned int isWhite(const uchar4 d_imgPixel) {
    return d_imgPixel.x == 255 && d_imgPixel.y == 255 && d_imgPixel.z == 255;
}

__global__ void separateIntoInteriodBorderExterior(const uchar4* d_img,
                                                   const size_t numRowsSource, const size_t numColsSource,
                                                   unsigned int* d_status
                                                  ) {
    const unsigned int row = (blockIdx.x * blockDim.x) + threadIdx.x;
    const unsigned int col = (blockIdx.y * blockDim.y) + threadIdx.y;
    if (row >= numRowsSource || col >= numColsSource) {
        return;
    }
    const unsigned int idx = row * numColsSource + col;
    if (isWhite(d_img[idx])) {
        d_status[idx] = exterior;
    } else {
        unsigned int hasWhiteNeighbour = false;
        for (unsigned int rowOffset = 0 ; rowOffset <= 2 ; rowOffset++) {
            if (hasWhiteNeighbour) {
                break;
            }
            for (unsigned int colOffset = 0 ; colOffset <= 2 ; colOffset++) {
                if (rowOffset == 1 && colOffset == 1) {
                    continue;
                } else if (rowOffset != 1 && colOffset != 1) {
                    continue;
                } else if (rowOffset + row == 0) {
                    continue;
                } else if (colOffset + col == 0) {
                    continue;
                } else if (rowOffset + row == numRowsSource + 1) {
                    continue;
                } else if (colOffset + col == numColsSource + 1) {
                    continue;
                } else {
                    const unsigned int neighbourRow = row + rowOffset - 1;
                    const unsigned int neighbourCol = col + colOffset - 1;
                    const unsigned int neighbourIdx = neighbourRow * numColsSource + neighbourCol;
                    if (isWhite(d_img[neighbourIdx])) {
                        hasWhiteNeighbour = true;
                        break;
                    }
                }
            }
        }
        if (hasWhiteNeighbour) {
            d_status[idx] = border;
        } else {
            d_status[idx] = interior;
        }
    }      
}

__global__ void visualizeImgStatus(const unsigned int* d_status, 
                                   const size_t numRowsSource, const size_t numColsSource,
                                   uchar4* d_imgToDraw) {
    const unsigned int row = (blockIdx.x * blockDim.x) + threadIdx.x;
    const unsigned int col = (blockIdx.y * blockDim.y) + threadIdx.y;
    if (row >= numRowsSource || col >= numColsSource) {
        return;
    }
    const unsigned int idx = row * numColsSource + col;
    if (d_status[idx] == interior) {
        d_imgToDraw[idx].x = 255;
        d_imgToDraw[idx].y = 0;
        d_imgToDraw[idx].z = 0;
    } else if (d_status[idx] == border) {
        d_imgToDraw[idx].x = 0;
        d_imgToDraw[idx].y = 255;
        d_imgToDraw[idx].z = 0;
    } else if (d_status[idx] == exterior) {
        d_imgToDraw[idx].x = 0;
        d_imgToDraw[idx].y = 0;
        d_imgToDraw[idx].z = 255;
    } else {
        printf("visualizeImgStatus problem at %d, %d", row, col);
    }
        
}

__global__ void copyToImageBuffer(const uchar4* d_img,
                                  const size_t numRowsSource, const size_t numColsSource,
                                  ImageBuffer* d_imgBfr) {
    const unsigned int row = (blockIdx.x * blockDim.x) + threadIdx.x;
    const unsigned int col = (blockIdx.y * blockDim.y) + threadIdx.y;
    if (row >= numRowsSource || col >= numColsSource) {
        return;
    }
    const unsigned int idx = row * numColsSource + col;
    d_imgBfr[idx].x = (float) d_img[idx].x;
    d_imgBfr[idx].y = (float) d_img[idx].y;
    d_imgBfr[idx].z = (float) d_img[idx].z;
}

dim3 getBlockSizeWithSeparation(const size_t numRowsSource, const size_t numColsSource, size_t* numColsSourceSeparatedPtr) {
    *numColsSourceSeparatedPtr = 3 * THREAD_PER_BLOCK_1D * divideAndRoundUp(numColsSource, THREAD_PER_BLOCK_1D);
    return dim3(divideAndRoundUp(numRowsSource, THREAD_PER_BLOCK_1D),
                                         divideAndRoundUp(*numColsSourceSeparatedPtr, THREAD_PER_BLOCK_1D));    
}

__device__ void doOneJacobiIteration(const unsigned int numberOfColumnsPerChannel,
                                     const unsigned int* d_status,
                                     uchar4* d_sourceImg,
                                     uchar4* d_destImg,
                                     const size_t numRowsSource, const size_t numColsSource,
                                     ImageBuffer* d_imageGuess_prev,
                                     ImageBuffer* d_imageGuess_next) {
    unsigned int row = (blockIdx.x * blockDim.x) + threadIdx.x;
    unsigned int col = (blockIdx.y * blockDim.y) + threadIdx.y;
    const unsigned int channelIdx = col / numberOfColumnsPerChannel;
    col %= numberOfColumnsPerChannel;

    if (row >= numRowsSource || col >= numColsSource) {
        return;
    }
    
    const unsigned int myIdx = row * numColsSource + col;    
    
    if (d_status[myIdx] == interior) {
        float sum1 = 0;
        float sum2 = 0;
        float numberOfNeightbours = 0;
        for (unsigned int rowOffset = 0 ; rowOffset <= 2 ; rowOffset++) {
            for (unsigned int colOffset = 0 ; colOffset <= 2 ; colOffset++) {
                if (rowOffset == 1 && colOffset == 1) {
                    continue;
                } else if (rowOffset != 1 && colOffset != 1) {
                    continue;
                } else if (rowOffset + row == 0) {
                    continue;
                } else if (colOffset + col == 0) {
                    continue;
                } else if (rowOffset + row == numRowsSource + 1) {
                    continue;
                } else if (colOffset + col == numColsSource + 1) {
                    continue;
                } else {
                    const unsigned int neighbourRow = row + rowOffset - 1;
                    const unsigned int neighbourCol = col + colOffset - 1;
                    const unsigned int neighbourIdx = neighbourRow * numColsSource + neighbourCol;

    /*
   Follow these steps to implement one iteration:

   1) For every pixel p in the interior, compute two sums over the four neighboring pixels:
      Sum1: If the neighbor is in the interior then += ImageGuess_prev[neighbor]
             else if the neighbor in on the border then += DestinationImg[neighbor]

      Sum2: += SourceImg[p] - SourceImg[neighbor]   (for all four neighbors)

   2) Calculate the new pixel value:
      float newVal= (Sum1 + Sum2) / 4.f  <------ Notice that the result is FLOATING POINT
      ImageGuess_next[p] = min(255, max(0, newVal)); //clamp to [0, 255]


    In this assignment we will do 800 iterations.
   */                    
                    if (d_status[neighbourIdx] == interior) {
                        if (channelIdx == 0) {
                            sum1 += d_imageGuess_prev[neighbourIdx].x;
                        } else if (channelIdx == 1) {
                            sum1 += d_imageGuess_prev[neighbourIdx].y;
                        } else if (channelIdx == 2) {
                            sum1 += d_imageGuess_prev[neighbourIdx].z;
                        }
                    } else if (d_status[neighbourIdx] == border) {
                        if (channelIdx == 0) {
                            sum1 += (float) d_destImg[neighbourIdx].x;
                        } else if (channelIdx == 1) {
                            sum1 += (float)  d_destImg[neighbourIdx].y;
                        } else if (channelIdx == 2) {
                            sum1 += (float) d_destImg[neighbourIdx].z;
                        }                        
                    }
                    
                    if (channelIdx == 0) {
                        sum2 += ((float) d_sourceImg[myIdx].x) - ((float) d_sourceImg[neighbourIdx].x);
                    } else if (channelIdx == 1) {
                        sum2 += ((float) d_sourceImg[myIdx].y) - ((float) d_sourceImg[neighbourIdx].y);
                    } else if (channelIdx == 2) {
                        sum2 += ((float) d_sourceImg[myIdx].z) - ((float) d_sourceImg[neighbourIdx].z);
                    } 
                    
                    numberOfNeightbours += 1.0;
                }
            }
        }
        
        float newVal= (sum1 + sum2) / numberOfNeightbours;
        float newValClamped = min(255.f, max(0.f, newVal));
        if (channelIdx == 0) {
            d_imageGuess_next[myIdx].x = newValClamped;
        } else if (channelIdx == 1) {
            d_imageGuess_next[myIdx].y = newValClamped;
        } else if (channelIdx == 2) {
            d_imageGuess_next[myIdx].z = newValClamped;
        }       
    }
}

__global__ void oneJacobiIteration(const unsigned int iteration,
                                   const unsigned int numberOfColumnsPerChannel,
                                   const unsigned int* d_status,
                                   uchar4* d_sourceImg,
                                   uchar4* d_destImg,
                                   const size_t numRowsSource, const size_t numColsSource,
                                   ImageBuffer* d_sourceImgBuffer1,
                                   ImageBuffer* d_sourceImgBuffer2) {
    if (iteration % 2 == 0) {
        doOneJacobiIteration(numberOfColumnsPerChannel,
                             d_status,
                             d_sourceImg,
                             d_destImg,
                             numRowsSource, numColsSource,
                             d_sourceImgBuffer1, d_sourceImgBuffer2);
    } else {
        doOneJacobiIteration(numberOfColumnsPerChannel,
                             d_status,
                             d_sourceImg,
                             d_destImg,
                             numRowsSource, numColsSource,
                             d_sourceImgBuffer2, d_sourceImgBuffer1);        
    }
}

__global__ void mergeImg(const unsigned int* d_status,
                    const uchar4* d_destImg,
                    uchar4* d_blendedImg,
                   const size_t numRowsSource, const size_t numColsSource,
                   ImageBuffer* d_sourceImgBuffer) {
    const unsigned int row = (blockIdx.x * blockDim.x) + threadIdx.x;
    const unsigned int col = (blockIdx.y * blockDim.y) + threadIdx.y;
    if (row >= numRowsSource || col >= numColsSource) {
        return;
    }
    const unsigned int myIdx = row * numColsSource + col;
    if (d_status[myIdx] == interior) {
        d_blendedImg[myIdx].x = (unsigned int) d_sourceImgBuffer[myIdx].x;
        d_blendedImg[myIdx].y = (unsigned int) d_sourceImgBuffer[myIdx].y;
        d_blendedImg[myIdx].z = (unsigned int) d_sourceImgBuffer[myIdx].z;
    } else {
        d_blendedImg[myIdx] = d_destImg[myIdx];
    }
}

void jacobiIteration(const unsigned int numberOfIterations,
                     const unsigned int* d_status,
                     uchar4* d_sourceImg,
                     uchar4* d_destImg,
                     uchar4* d_blendedImg,
                     const size_t numRowsSource, const size_t numColsSource,
                     ImageBuffer* d_sourceImgBuffer1,
                     ImageBuffer* d_sourceImgBuffer2) {
    const dim3 gridSize = dim3(THREAD_PER_BLOCK_1D, THREAD_PER_BLOCK_1D);
    size_t numColsSourceSeparated;
    const dim3 blockSizeWithSeparation = getBlockSizeWithSeparation(numRowsSource, numColsSource, &numColsSourceSeparated);
    const size_t numberOfColumnsPerChannel = numColsSourceSeparated / 3;
    
    for (unsigned int iter = 0 ; iter < numberOfIterations ; iter++) {
         oneJacobiIteration<<<blockSizeWithSeparation, gridSize>>>(iter,
                                   numberOfColumnsPerChannel,
                                   d_status,
                                   d_sourceImg,
                                   d_destImg,
                                   numRowsSource, numColsSource,
                                   d_sourceImgBuffer1,
                                   d_sourceImgBuffer2);
    }
    
    const dim3 blockSizeNoSeparation = dim3(divideAndRoundUp(numRowsSource, THREAD_PER_BLOCK_1D),
                                         divideAndRoundUp(numColsSource, THREAD_PER_BLOCK_1D));    
    
    if ((numberOfIterations - 1) % 2 == 0) {
        mergeImg<<<blockSizeNoSeparation, gridSize>>>(d_status,
                    d_destImg,
                    d_blendedImg,
                    numRowsSource, numColsSource,
                    d_sourceImgBuffer2);
    } else {
        mergeImg<<<blockSizeNoSeparation, gridSize>>>(d_status,
                    d_destImg,
                    d_blendedImg,
                    numRowsSource, numColsSource,
                    d_sourceImgBuffer1);        
    }
    
}

void your_blend(const uchar4* const h_sourceImg,  //IN
                const size_t numRowsSource, const size_t numColsSource,
                const uchar4* const h_destImg, //IN
                uchar4* const h_blendedImg) //OUT
{
    const unsigned int NUMBER_OF_ELEMENTS = numRowsSource * numColsSource;
    
    printf("Welcome\n");
    printf("numRowsSource = %d, numColsSource = %d\n", numRowsSource, numColsSource);    
    printf("NUMBER_OF_ELEMENTS = %d\n", NUMBER_OF_ELEMENTS);
    
    const dim3 gridSize = dim3(THREAD_PER_BLOCK_1D, THREAD_PER_BLOCK_1D);
    const dim3 blockSizeNoSeparation = dim3(divideAndRoundUp(numRowsSource, THREAD_PER_BLOCK_1D),
                                         divideAndRoundUp(numColsSource, THREAD_PER_BLOCK_1D));
    
    uchar4* d_sourceImg;
    checkCudaErrors(cudaMalloc(&d_sourceImg, sizeof(uchar4) * NUMBER_OF_ELEMENTS));
    checkCudaErrors(cudaMemcpy((void*) d_sourceImg, (void*) h_sourceImg, sizeof(uchar4) * NUMBER_OF_ELEMENTS, cudaMemcpyHostToDevice));
    
    uchar4* d_destImg;
    checkCudaErrors(cudaMalloc(&d_destImg, sizeof(uchar4) * NUMBER_OF_ELEMENTS));
    checkCudaErrors(cudaMemcpy((void*) d_destImg, (void*) h_destImg, sizeof(uchar4) * NUMBER_OF_ELEMENTS, cudaMemcpyHostToDevice));
    
    uchar4* d_blendedImg;
    checkCudaErrors(cudaMalloc(&d_blendedImg, sizeof(uchar4) * NUMBER_OF_ELEMENTS));
    
    unsigned int* d_sourceImg_status;
    checkCudaErrors(cudaMalloc(&d_sourceImg_status, sizeof(unsigned int) * NUMBER_OF_ELEMENTS));
    separateIntoInteriodBorderExterior<<<blockSizeNoSeparation, gridSize>>>
        (d_sourceImg, numRowsSource, numColsSource, d_sourceImg_status);
    
    /*
    visualizeImgStatus<<<blockSizeNoSeparation, gridSize>>>(d_sourceImg_status, 
                                   numRowsSource, numColsSource,
                                   d_blendedImg);
    */
    ImageBuffer* d_sourceImgBuffer1;
    checkCudaErrors(cudaMalloc(&d_sourceImgBuffer1, sizeof(ImageBuffer) * NUMBER_OF_ELEMENTS));
    copyToImageBuffer<<<blockSizeNoSeparation, gridSize>>>(d_sourceImg,
                                  numRowsSource, numColsSource,
                                  d_sourceImgBuffer1);
    ImageBuffer* d_sourceImgBuffer2;
    checkCudaErrors(cudaMalloc(&d_sourceImgBuffer2, sizeof(ImageBuffer) * NUMBER_OF_ELEMENTS));
    copyToImageBuffer<<<blockSizeNoSeparation, gridSize>>>(d_sourceImg,
                                  numRowsSource, numColsSource,
                                  d_sourceImgBuffer2);

    jacobiIteration(800, d_sourceImg_status,
                    d_sourceImg, d_destImg, d_blendedImg,
                    numRowsSource, numColsSource,
                    d_sourceImgBuffer1, d_sourceImgBuffer2);  
    
    
    checkCudaErrors(cudaMemcpy((void*) h_blendedImg, (void*) d_blendedImg, sizeof(uchar4) * NUMBER_OF_ELEMENTS, cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaFree(d_sourceImg));
    checkCudaErrors(cudaFree(d_destImg));
    checkCudaErrors(cudaFree(d_blendedImg));
    checkCudaErrors(cudaFree(d_sourceImg_status));
    checkCudaErrors(cudaFree(d_sourceImgBuffer1));
    checkCudaErrors(cudaFree(d_sourceImgBuffer2));
  /* To Recap here are the steps you need to implement
  
     1) Compute a mask of the pixels from the source image to be copied
        The pixels that shouldn't be copied are completely white, they
        have R=255, G=255, B=255.  Any other pixels SHOULD be copied.

     2) Compute the interior and border regions of the mask.  An interior
        pixel has all 4 neighbors also inside the mask.  A border pixel is
        in the mask itself, but has at least one neighbor that isn't.

     3) Separate out the incoming image into three separate channels

     4) Create two float(!) buffers for each color channel that will
        act as our guesses.  Initialize them to the respective color
        channel of the source image since that will act as our intial guess.

     5) For each color channel perform the Jacobi iteration described 
        above 800 times.

     6) Create the output image by replacing all the interior pixels
        in the destination image with the result of the Jacobi iterations.
        Just cast the floating point values to unsigned chars since we have
        already made sure to clamp them to the correct range.

      Since this is final assignment we provide little boilerplate code to
      help you.  Notice that all the input/output pointers are HOST pointers.

      You will have to allocate all of your own GPU memory and perform your own
      memcopies to get data in and out of the GPU memory.

      Remember to wrap all of your calls with checkCudaErrors() to catch any
      thing that might go wrong.  After each kernel call do:

      cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

      to catch any errors that happened while executing the kernel.
  */



  /* The reference calculation is provided below, feel free to use it
     for debugging purposes. 
   */

  /*
    uchar4* h_reference = new uchar4[srcSize];
    reference_calc(h_sourceImg, numRowsSource, numColsSource,
                   h_destImg, h_reference);

    checkResultsEps((unsigned char *)h_reference, (unsigned char *)h_blendedImg, 4 * srcSize, 2, .01);
    delete[] h_reference; */
}

